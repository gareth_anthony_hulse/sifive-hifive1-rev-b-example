/* © Copyright 2020 Gareth Anthony Hulse
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "app/prci.hpp"
#include "app/util.hpp"
#include "app/clint.hpp"

#include <matilda/cpu/sifive/e31.hpp>
#include <cstdio>
#include <cstdint>
#include <array>

namespace
{
  namespace cpu = matilda::cpu::sifive::e31;
  
  cpu::gpio gpio;
  cpu::spi spi_1 (1);

  /*  void
  setup_prci ()
  {
    app::prci prci;

    prci.hfrosc_cfg (app::prci::bitmask::hfrosc_cfg::enable);

    prci.pll_cfg (app::prci::bitmask::pll_cfg::reference_select | app::prci::bitmask::pll_cfg::select  | 0b1 | 39 << 4 | 1 << 10);
    app::util::delay (app::util::seconds (3U));
  }
  */
  void
  setup_spi ()
  {
    spi_1.csdef (0xffffffff);
    spi_1.csid (0b1);
    spi_1.delay0 (0b0, 0b0);
    spi_1.delay1 (0b0 , 0b0);

    spi_1.fmt (cpu::spi::bitmask::fmt::proto::single, 0, 0, 8);
    spi_1.sckdiv (79); // cpu_clock_speed / (target_frequency * 2) - 1
    spi_1.csmode (cpu::spi::bitmask::csmode::mode::off);
    gpio.iof_en (1 << 2 | 1 << 8 | 1 << 9 | 1 << 10); // Enable SPI.
    app::util::delay (app::util::seconds (1U));
  }
}

int
main ()
{
  //setup_prci (); // Comment out for now, until UART is sorted out.
  setup_spi ();
  
  spi_1.csdef (0xfffffffe); // Enable.
  spi_1.txdata (0b0000'0010); // Write to 'Mode' register.
  spi_1.txdata (0b0000'0000); // 'Mode' register is 16 bits wide.
  spi_1.txdata (0b0000'0011); // set bits 'MD1' and 'MD0' high, to put the chip in standby mode.
  uint32_t data = spi_1.txdata (0b0001'0000); // read 'Status' register once.
  spi_1.txdata (0b0001'0000); // read 'Status' register once.
  spi_1.csdef (0xffffffff); // Disable.

  printf ("%lu\n", data);
  
  return 0;
}
