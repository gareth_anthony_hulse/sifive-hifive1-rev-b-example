/* © Copyright 2020 Gareth Anthony Hulse
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "core.hpp"
#include "clint.hpp"

#include <cmath>
#include <cstdio>

namespace app
{
  class util
  {
  private:
    inline static app::clint m_clint;

  public:
    static void
    delay (uint64_t time)
    {
      time = time + m_clint.mtime ();
      while (m_clint.mtime () < time);
    }

    static constexpr uint64_t
    seconds (const uint64_t time)
    {
      return time * static_cast<uint64_t> (std::pow (10, 9) / app::clint::clock);
    }

    static constexpr uint64_t
    miliseconds (const uint64_t time)
    {
      return time * static_cast<uint64_t> (std::pow (10, 6) / app::clint::clock);
    }
  };
}
