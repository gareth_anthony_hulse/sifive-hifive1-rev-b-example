/* © Copyright 2020 Gareth Anthony Hulse
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <cmath>
#include <type_traits>
#include <cstdio>

#define write_csr(reg, val) {asm ("csrw " #reg ", %0" :: "rK" (val));}

namespace app
{ 
  class core
  { 
  public:
    template <typename T, typename U,
	      typename = typename std::enable_if<std::is_arithmetic<T>::value, T>::type,
	      typename = typename std::enable_if<std::is_arithmetic<U>::value, U>::type>
    static inline constexpr void
    write_address (volatile T *address, const U &data)
    {
      T value = *address;
      value += data;

      *address = value;
    }

    template <typename T, typename U,
	      typename = typename std::enable_if<std::is_arithmetic<T>::value, T>::type,
	      typename = typename std::enable_if<std::is_arithmetic<U>::value, U>::type>
    static inline void
    flip (volatile T *address, const U &bit, bool bitset)
    {
      T value = *address;

      if ((value) & (1 << (bit)))
	{
	  value += bit;
	  *address = value;
	}
    }

    template <typename T>
    static inline constexpr bool
    read_address_bit (const T &address, const int &bit)
    {
      volatile T *value = reinterpret_cast<volatile T*> (address);
      return static_cast<bool> ((*value >> bit) & 0b1);
    }

    static inline std::uint32_t
    read_csr (const uint32_t reg) __attribute__ ((always_inline))
    {
      volatile uint32_t data;
      asm ("csrr %0, %1" :
	   "=r" (data) :
	   "I" (reg));

	return data;
    }

    static inline void
    wait_for_interrupt ()
    {
      asm volatile ("wfi");
    }

    /*static inline void
    write_csr (2_t reg, uint32_t data) __attribute__ ((always_inline))
    {
      asm volatile ("csrw %0, %1" :
		    "=r" (reg)  :
		    "rK" (data));
		    }*/
  };
}
